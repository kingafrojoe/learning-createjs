(function (window) {
    function Brick(currCol, currRow, rectWidth, rectHeight, stroke, colour) {
        this.initialize(currCol, currRow, rectWidth, rectHeight, stroke, colour);
    }
    Brick.prototype = new createjs.Shape();

    Brick.prototype.Bitmap_initialize = Brick.prototype.initialize;
   
    Brick.prototype.initialize = function (currCol, currRow, rectWidth, rectHeight, stroke, colour) {
		
		this.graphics.beginStroke(stroke).beginFill(colour).drawRect(currCol, currRow, rectWidth, rectHeight);
		
		
    }

    window.Brick = Brick;
} (window));
/*
(function() {

	var Brick = function(currCol, currRow, rectWidth, rectHeight, stroke, colour) {
	  this.initialize(currCol, currRow, rectWidth, rectHeight, stroke, colour);
	}
	var p = Brick.prototype = new createjs.Container(); // inherit from Container
	
	p.label;
	p.background;
	p.count = 0;
	
	p.Container_initialize = p.initialize;
	p.initialize = function(currCol, currRow, rectWidth, rectHeight, stroke, colour) {
		this.Container_initialize();
		
		
		this.background = new createjs.Shape();
		this.background.graphics.beginStroke(stroke).beginFill(colour).drawRect(currCol, currRow, rectWidth, rectHeight);
		
		this.addChild(this.background); 
	
	
	} 
	
	window.Brick = Brick;
}());
*/