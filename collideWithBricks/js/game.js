var		BALL_IMAGE = 'assets/ball.png',
		PLATFORM_IMAGE = 'assets/paddle.png',
		BRICK_IMAGE = 'assets/brick.png',
		stage;

function _game()
{
	window.Game = this;
	var self = this,
		ticks = 0,
		canvas,
		world,
		ball,
		platform,
		w = 450,//getWidth(),
		h = getHeight(),
		assets = [],
		keyDown = false,
		brickArr = [];

	// holds all collideable objects
	var collideables = [];
	this.getCollideables = function() { return collideables; };
	
	this.getBricks = function() { return brickArr; };

	// starts to load all the assets
	this.preloadResources = function() {
		self.loadImage(BALL_IMAGE);
		self.loadImage(PLATFORM_IMAGE);
		self.loadImage(BRICK_IMAGE);
	}

	var requestedAssets = 0,
		loadedAssets = 0;
	// loads the assets and keeps track 
	// of how many assets where there to
	// be loaded
	this.loadImage = function(e) {
		var img = new Image();
		img.onload = self.onLoadedAsset;
		img.src = e;

		assets[e] = img;

		++requestedAssets;
	}
	// each time an asset is loaded
	// check if all assets are complete
	// and initialize the game, if so
	this.onLoadedAsset = function(e) {
		++loadedAssets;
		if ( loadedAssets == requestedAssets ) {
			self.initializeGame();
		}
	}

	this.initializeGame = function() {
		// creating the canvas-element
		canvas = document.createElement('canvas');
		canvas.width = w;
		canvas.height = h;
		document.body.appendChild(canvas);
		//canvas.style.margin = '0 auto';
		

		// initializing the stage
		stage = new createjs.Stage(canvas);
		world = new createjs.Container();
		stage.addChild(world);
		
		console.log(stage);
		
		
		// number of bricks
		var horizontalNumber = 7; 
		var verticalNumber = 5;
		
		// size of bricks 
		var rectWidth = w / horizontalNumber;
		var rectHeight = ( h / 3 ) / verticalNumber;
		
		//inital values
		var currRow = 0,
			currCol = 0,			
			index = 0;
		
		for(var row=0;row<verticalNumber;row++){ // for each row
			for(var col=0;col<horizontalNumber;col++){ // for each column
				var stroke = '#333333';
				var colour = '#ff0000';
				// create brick into array.
				//var brick = new Brick(currCol, currRow, rectWidth, rectHeight, stroke, colour);			
				self.addBrick(currCol, currRow);
				
				// add brick next to prevous brick					
				currCol = currCol + rectWidth;
				// brick index
				index++;
			}
			// reset column for next row
			currCol = 0;
			// update next row.
			currRow = currRow + rectHeight;
		}
		
		
		// creating the Ball, and assign an image
		// also position the ball in the middle of the screen
		ball = new Ball(assets[BALL_IMAGE]);
		ball.x = w/2
		ball.y = h/2;
		world.addChild(ball);

		// add a platform for the ball to collide with
		self.addPlatform(w/2 - assets[PLATFORM_IMAGE].width/2, h/1.25);

		// Setting the listeners
		if ('ontouchstart' in document.documentElement) {
			canvas.addEventListener('touchstart', function(e) {
				self.handleKeyDown();
			}, false);

			canvas.addEventListener('touchend', function(e) {
				self.handleKeyUp();
			}, false);
		} else {
			document.onkeydown = self.handleKeyDown;
			document.onkeyup = self.handleKeyUp;
			document.onmousedown = self.handleKeyDown;
			document.onmouseup = self.handleKeyUp;
		}
		
		Ticker = createjs.Ticker;
		Ticker.setFPS(30);
		Ticker.on("tick", self.tick);
	}

	this.tick = function(e)
	{
		ticks++;
		ball.tick();
		self.movePlatform();
		stage.update();
	}
	
	// this method adds a platform at the
	// given x- and y-coordinates and adds
	// it to the collideables-array
	this.addPlatform = function(x,y) {
		x = Math.round(x);
		y = Math.round(y);

		platform = new createjs.Bitmap(assets[PLATFORM_IMAGE]);
		platform.x = x;
		platform.y = y;
		platform.snapToPixel = true;

		world.addChild(platform);
		collideables.push(platform);
		
		console.log(platform);
	}
	
	this.movePlatform = function(){
		stage.on("stagemousemove",function(evt) {
			//console.log(platform);
			var half = platform.image.width / 2;
			platform.x = evt.stageX - half;
		});
	}
	
	//
	// add brick to stage
	this.addBrick = function(x, y) {
		
		var brick = new createjs.Bitmap(assets[BRICK_IMAGE]);
		brick.x = x;
		brick.y = y;
		brick.snapToPixel = true;

		world.addChild(brick);
		
		brickArr.push(brick);

		console.log(brick);
		
	
	}

	this.handleKeyDown = function(e)
	{
		if ( !keyDown ) {
			keyDown = true;
			//ball.jump();
		}
	}

	this.handleKeyUp = function(e)
	{
		keyDown = false;
	}

	self.preloadResources();
};

new _game();