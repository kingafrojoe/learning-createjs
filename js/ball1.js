(function (window) {
	function Ball(image) {
		this.initialize(image);
	}
	Ball.prototype = new createjs.Bitmap();

	// save the original initialize-method so it won't be gone after
	// overwriting it
	Ball.prototype.Bitmap_initialize = Ball.prototype.initialize;

	// initialize the object
	Ball.prototype.initialize = function (image) {
		this.velocity = {x:0,y:12};
		this.Bitmap_initialize(image);
		this.name = 'Ball';
		this.snapToPixel = true;
	}

	// we will call this function every frame to 
	Ball.prototype.tick = function () {
		this.velocity.y += 1;
		this.y += this.velocity.y;
	}

	// this will reset the position of the Ball
	// we can call this e.g. whenever a key is pressed
	Ball.prototype.reset = function() {
		this.x = 500 / 2;
		this.y = 300 / 1.25;
		this.velocity.y = -15;
	}

	window.Ball = Ball;
} (window));