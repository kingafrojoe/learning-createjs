(function (window) {
    function Ball(image) {
        this.initialize(image);
    }
    Ball.prototype = new createjs.Bitmap();

    Ball.prototype.Bitmap_initialize = Ball.prototype.initialize;
   
    Ball.prototype.initialize = function (image) {
       	this.velocity = {x:0,y:25};
       	this.onGround = false;
		this.doubleJump = false;

        this.Bitmap_initialize(image);
        this.name = 'Ball';
        this.snapToPixel = true;
		this.yDirection = 'down';
		this.xDirection = null;
    }

    Ball.prototype.tick = function () {
        
		//move ball up or down
		if(this.yDirection == 'down'){
			this.velocity.y += 1;
		}else{
			this.velocity.y -= 1;	
		}
		//move ball left right or null
		if(this.xDirection){
			if(this.xDirection == 'left'){
				this.velocity.x -= 1;
			}else{
				this.velocity.x += 1;	
			}	
		}else{
			this.velocity.x = 0;	
		}
		
        // preparing the variables
		var c = 0,
			cc = 0,
			addY = this.velocity.y,
			addX = this.velocity.x,
			bounds = getBounds(this),
			cbounds,
			collision = null,
			collideables = Game.getCollideables(),
			stageBounds = stageBoundsCollide(stage,this);
			
		
		if(stageBounds){
			if(stageBounds.direction == 'right'){
				this.velocity.x = 7;
				this.xDirection = 'right';
			}
			if(stageBounds.direction == 'left'){
				this.velocity.x = -7;
				this.xDirection = 'left';
			}
			if(stageBounds.direction == 'up'){
				this.velocity.y = -7;
				this.yDirection = 'up';
			}
			if(stageBounds.direction == 'down'){
				this.velocity.y = 7;
				this.yDirection = 'down';
			}
			//console.log(stageBounds);
		}
		
		
		cc=0;
		// for each collideable object we will calculate the
		// bounding-rectangle and then check for an intersection
		// of the ball's future position's bounding-rectangle
		while ( !collision && cc < collideables.length ) {
			cbounds = getBounds(collideables[cc]);
			if ( collideables[cc].isVisible ) {
				collision = calculateIntersection(bounds, cbounds, addX, addY);
			}
			
			if(collision){
				//console.log(collision);
				if( collision.ball.cx <= ( collision.paddle.cx - 10 ) ){
					console.log('left');
					this.velocity.x = -7;
					this.xDirection = 'left';
				}
				if( collision.ball.cx >= ( collision.paddle.cx + 10 ) ){
					console.log('right');
					this.velocity.x = 7;
					this.xDirection = 'right';	
				}		
				if( collision.ball.cx > ( collision.paddle.cx - 10 ) && collision.ball.cx < ( collision.paddle.cx + 10 ) ){
					console.log('middle');
					this.velocity.x = 7;
					this.xDirection = null;	
				}
			}

			if ( !collision && collideables[cc].isVisible ) {
				// if there was NO collision detected, but somehow
				// the ball got onto the "other side" of an object (high velocity e.g.),
				// then we will detect this here, and adjust the velocity according to
				// it to prevent the Ball from "ghosting" through objects
				// try messing with the 'this.velocity = {x:0,y:25};'
				// -> it should still collide even with very high values
				
				
				if ( ( bounds.y < cbounds.y && bounds.y + addY > cbounds.y )
				  || ( bounds.y > cbounds.y && bounds.y + addY < cbounds.y ) ) {
					addY = cbounds.y - bounds.y;
				} else {
					cc++;
				}
				
			}
		}

		// if no collision was to be found, just
		//  move the ball to it's new position
		if ( !collision ) {
			this.y += addY;
			this.x += addX;
			if ( this.onGround ) {
				this.onGround = false;
				this.doubleJump = true;
			}
		// else move the ball as far as possible
		// and then make it stop and tell the
		// game, that the ball is now "at the ground"
		} else {
			this.y += addY;
			this.x += addX;
			if ( addY > 0 ) {
				this.onGround = true;
				this.doubleJump = false;
			}
			this.velocity.y = -17;
			this.yDirection = 'up';
		}
    }

    Ball.prototype.jump = function() {
    	// if the ball is "on the ground"
    	// let him jump, physically correct!
		if ( this.onGround ) {
			this.velocity.y = -17;
			this.onGround = false;
			this.doubleJump = true;
		// we want the ball to be able to
		// jump once more when he is in the
		// air - after that, he has to wait
		// to lang somewhere on the ground
		} else if ( this.doubleJump ) {
			this.velocity.y = -17;
			this.doubleJump = false;
		}
	}

    window.Ball = Ball;
} (window));